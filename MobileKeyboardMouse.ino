#include <ArduinoBLE.h>
#include <Arduino_LSM6DS3.h>
#include "Keyboard.h"
#include "Mouse.h"

BLEService KeyBoardService("19B10000-E8F2-537E-4F6C-D104768A1214");
BLEUnsignedIntCharacteristic inputData("19B10001-E8F2-537E-4F6C-D104768A1214", BLERead | BLEWrite);

uint8_t keycode;
uint8_t behavior;
uint8_t keytype;

enum {
  KEYBOARD = 1,
  MOUSE_BUTTON,
  MOUSE_MOVE,
  MOUSE_WHEEL
};

void setup() {
  Serial.begin(9600);
  Keyboard.begin();
  Mouse.begin();

  if (!BLE.begin()) {
    Serial.println("BLE initialize failed, please reboot!");
    while (1)
      ;
  }

  if (!IMU.begin()) {
    Serial.println("IMU initialize failed, please reboot!");

    while (1)
      ;
  }

  BLE.setLocalName("JiYen-IOT");
  BLE.setAdvertisedService(KeyBoardService);
  KeyBoardService.addCharacteristic(inputData);
  BLE.addService(KeyBoardService);
  BLE.advertise();
  Serial.println("The device name is JiYen-IOT");
}

void mouse_detect() {
  float x, y, z;

  if (IMU.accelerationAvailable()) {
    IMU.readAcceleration(x, y, z);

    if (0.5 < abs(y)) {
      Mouse.move(10 * y, 0, 0);
    }
    if (0.5 < abs(x)) {
      Mouse.move(0, 10 * x, 0);
    }
  }
}

void loop() {
  BLEDevice central = BLE.central();

  if (central) {
    Serial.print("Connected to central: ");
    Serial.println(central.address());

    while (central.connected()) {
      if (inputData.written() && inputData.value()) {
        keycode = inputData.value() & 255;
        behavior = inputData.value() >> 8;
        keytype = inputData.value() >> 16;
        if (KEYBOARD == keytype) {
          if (KEY_LEFT_CTRL == keycode || KEY_LEFT_SHIFT == keycode || KEY_LEFT_ALT == keycode || KEY_LEFT_GUI == keycode) {
            if (behavior) {
              Keyboard.press(keycode);
              Serial.print("The key ");
              Serial.print(keycode);
              Serial.println(" is pushed");
            } else {
              Keyboard.release(keycode);
              Serial.print("The key ");
              Serial.print(keycode);
              Serial.println(" is released");
            }
          } else {
            if (behavior) {
              Keyboard.write(keycode);
              Serial.print("The key ");
              Serial.print(keycode);
              Serial.println(" is clicked");
            }
          }
        }
        if (MOUSE_BUTTON == keytype) {
          if (behavior) {
            Mouse.press(keycode);
            Serial.print("The button ");
            Serial.print(keycode);
            Serial.println(" is pushed");
          } else {
            Mouse.release(keycode);
            Serial.print("The button ");
            Serial.print(keycode);
            Serial.println(" is released");
          }
        }
        if (MOUSE_MOVE == keytype) {
          Serial.print("The mouse move x:");
          Serial.print(keycode - 128);
          Serial.print(" y:");
          Serial.println(behavior - 128);
          Mouse.move(keycode - 128, behavior - 128, 0);
        }
        if (MOUSE_WHEEL == keytype) {
          if (behavior) {
            Serial.println("The mouse scroll up");
          } else {
            Serial.println("The mouse scroll down");
          }
          Mouse.move(0, 0, (behavior - 1) * 10);
          Mouse.move(0, 0, 0);
        }
      }
    }

    Serial.print(F("Disconnected from central: "));
    Serial.println(central.address());
  }
}
