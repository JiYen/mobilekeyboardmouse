## Goal
This module allows you use your laptop keyboard and touch board or mouse to control other PC device. It is an easy solution to let you access the console of other devices for debugging or do some easy task and do not need to bring another keyboard or mouse.

## Requirement
- Arduino Nano 33 IoT
- A laptop with bluetooth functionality and python3 installed.

## Mechanism
This is an easy solution that a python-based daemon will listen to the global keyboard and mouse event and send this behavior to the arduino. Then, the arduino will send this event to the device you want to control.

## How to use
In the beginning, you need to download the arduino IDE to flash the code called MobileKeyboardMouse.ino. About the step, please follow the official document to flash the code.