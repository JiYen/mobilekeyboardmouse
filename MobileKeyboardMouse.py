import asyncio
import collections
import keyboard
import threading
import time
from bleak import BleakClient, BleakScanner
from pymouse import PyMouseEvent

DEVICE_NAME = "JiYen-IOT"
SERVICE_UUID = "19B10001-E8F2-537E-4F6C-D104768A1214"

key_map = dict([(0, 0),
                (1, 177),
                (2, 49),
                (3, 50),
                (4, 51),
                (5, 52),
                (6, 53),
                (7, 54),
                (8, 55),
                (9, 56),
                (10, 57),
                (11, 48),
                (12, 45),
                (13, 61),
                (14, 178),
                (15, 179),
                (16, 113),
                (17, 119),
                (18, 101),
                (19, 114),
                (20, 116),
                (21, 121),
                (22, 117),
                (23, 105),
                (24, 111),
                (25, 112),
                (26, 113),
                (27, 125),
                (43, 92),
                (58, 193),
                (30, 97),
                (31, 115),
                (32, 100),
                (33, 102),
                (34, 103),
                (35, 104),
                (36, 106),
                (37, 107),
                (38, 108),
                (39, 59),
                (40, 39),
                (28, 10),
                (42, 129),
                (44, 122),
                (45, 120),
                (46, 99),
                (47, 118),
                (48, 98),
                (49, 110),
                (50, 109),
                (51, 44),
                (52, 46),
                (53, 47),
                (54, 133),
                (29, 128),
                (91, 131),
                (56, 130),
                (57, 32),
                (72, 218),
                (80, 217),
                (75, 216),
                (77, 215),
                (73, 211),
                (81, 214),
                (71, 210),
                (79, 213),
                (83, 212),
                (301, 1),
                (302, 2),
                (303, 3)])

my_ble_addr = None
async def find_device():
    devices = await BleakScanner.discover()
    for device in devices:
        if device.name == DEVICE_NAME:
            global my_ble_addr
            my_ble_addr = device.address

asyncio.run(find_device())

button_event = collections.deque(maxlen = 128)
button_event_lock = threading.Lock()
async def connect_device():
    async with BleakClient(my_ble_addr) as client:
        print(SERVICE_UUID)
        while True:
            button_event_s = None
            with button_event_lock:
                if button_event:
                    button_event_s = button_event.pop()
            if button_event_s:
                try:
                    print(button_event_s)
                    if 3 != button_event_s[2]:
                        await client.write_gatt_char(SERVICE_UUID, f"{chr(key_map[button_event_s[0]])}{chr(button_event_s[1])}{chr(button_event_s[2])}".encode("latin1"))
                    else:
                        await client.write_gatt_char(SERVICE_UUID, f"{chr(button_event_s[0])}{chr(button_event_s[1])}{chr(button_event_s[2])}".encode("latin1"))
                except:
                    print("Not support key")


def connect_device_wrapper():
    try:
        if my_ble_addr:
            print(f"The MAC address of the IOT device is {my_ble_addr}")
        asyncio.run(connect_device())
    except:
        print("Can not detect the IOT device, please reboot the device.")


def keyboard_detect():
    while True:
        event = keyboard.read_event()
        if event.event_type == keyboard.KEY_DOWN:
            direction = 1
        else:
            direction = 0
        with button_event_lock:
            button_event.append((event.scan_code, direction, 1))

class mouse_detect(PyMouseEvent):
    def __init__(self):
        PyMouseEvent.__init__(self)
        self.sample_time = time.monotonic_ns()
        self.last_x = 0
        self.last_y = 0

    def click(self, x, y, button, press):
        button_event.append((button + 300, int(press), 2))
        return
        
    def move(self, x, y):
        if time.monotonic_ns() - self.sample_time > 100_000_000:
            self.sample_time = time.monotonic_ns()
            tmp_x = -128 if x - self.last_x < -128 else x - self.last_x
            tmp_x = 127 if tmp_x > 127 else tmp_x
            tmp_y = -128 if y - self.last_y < -128 else y - self.last_y
            tmp_y = 127 if tmp_y > 127 else tmp_y
            button_event.append((tmp_x + 128, tmp_y + 128, 3))
            self.last_x = x
            self.last_y = y
        return
        
    def scroll(self, x, y, direction, duplicated):
        button_event.append((0, direction + 1, 4))
        return
        
threading.Thread(target = connect_device_wrapper).start()
threading.Thread(target = keyboard_detect).start()
threading.Thread(target = mouse_detect().run).start()